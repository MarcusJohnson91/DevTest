<?php

/**
 * @author William Byrne <wbyrne@razoyo.com>
 * Documentation: https://github.com/razoyo/devtest
 */

require_once __DIR__ . '/raz-lib.php';
require_once __DIR__ . '/dev-lib.php';

$apiWsdl = 'https://www.shopjuniorgolf.com/api/?wsdl';
$formatKey = 'csv'; // csv, xml, or json
#putenv('RAZOYO_TEST_KEY=ku%64TeYMo5mAIFj8e');

// Connect to SOAP API using PHP's SoapClient class
// Feel free to create your own classes to organize code
$soap                = new SoapClient($apiWsdl);
$LoginDeets          = array('username' => 'devtest', 'apiKey' => 'ku%64TeYMo5mAIFj8e',);
$SessionToken        = $soap->__call('login', $LoginDeets);
$ProductList         = $soap->call($SessionToken, 'catalog_product.list');       // Get an array of attributes for each product in the store.
    
$SKUList             = array_column($ProductList, 'sku');
$NameList            = array_column($ProductList, 'name');
    
for ($Count = 0; $Count < count($SKUList, COUNT_RECURSIVE); $Count++) {
    $ProductInfo[$Count] = $soap->call($SessionToken, 'catalog_product.info', $SKUList[$Count]);
}
$PriceList              = array_column($ProductInfo, 'price');
$DescriptionList        = array_column($ProductInfo, 'short_description');

for ($Count = 0; $Count < count($SKUList, COUNT_RECURSIVE); $Count++) {
    $ProductRecords[$Count] = array($SKUList[$Count], $NameList[$Count], $PriceList[$Count], $DescriptionList[$Count]);
}
// You will need to create a FormatFactory.
$factory = new FormatFactory();
$format  = $factory->create($formatKey);

// See ProductOutput in raz-lib.php for reference
$output = new ProductOutput();
$output->setFormat($formatKey);
$output->setProducts($ProductRecords);
// ...
$output.format();
    
$soap->endsession($SessionToken);
