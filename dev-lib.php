<?php
    
    /*!
     @author Marcus Johnson
     @date   09/17/18
     */
    
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
    error_reporting(-1);
    
    class FormatFactory {
        const CSVFORMAT  = 1;
        const XMLFORMAT  = 2;
        const JSONFORMAT = 3;
        
        protected $FormatType    = 0;
        protected $NumRecords    = 0;
        protected $CurrentRecord = 0;
        
        public function create($FormatType) {
            if ($FormatType == 'csv') {
                $this->FormatType = CSVFORMAT;
            } elseif ($FormatType == 'xml') {
                $this->FormatType = XMLFORMAT;
            } elseif ($FormatType == 'json') {
                $this->FormatType = JSONFORMAT;
            } else {
                echo "Invalid FormatType $FormatType\n";
            }
        }
        
        public function formatProduct(array $Product) {
            if ($this->NumRecords == 0) {
                $this->NumRecords = count($Product, COUNT_RECURSIVE);
            }
            $this->CurrentRecord += 1;
            
            if ($this->CurrentRecord == 0) { // This is the first one so write all the start code
                if ($FormatType == CSVFORMAT) {
                    echo "SKU,Name,Price,Description\n";
                } elseif ($FormatType == XMLFORMAT) {
                    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"\?\>\n";
                    echo "\t<Records>";
                } elseif ($FormatType == JSONFORMAT) {
                    echo "{\n";
                    echo "\"Records\": {";
                }
            }
            
            if ($FormatType == CSVFORMAT) {
                echo "$Product[0],$Product[1],$Product[2],$Product[3]\n";
            } elseif ($FormatType == XMLFORMAT) {
                echo "<Record>";
                echo "\t\t<SKU>$Product[0]</SKU>\n";
                echo "\t\t<Name>$Product[1]</Name>\n";
                echo "\t\t<Price>$Product[2]</Price>\n";
                echo "\t\t<Description>$Product[3]</Description>";
                echo "</Record>";
            } elseif ($FormatType == JSONFORMAT) {
                echo "{\n";
                echo "\"Record\": {";
                echo "\"SKU\":\"$Product[0]\",\n";
                echo "\"Name\":\"$Product[1]\",\n";
                echo "\"Price\":$Product[2],\n";
                echo "\"Description\":\"$Product[3]\",\n";
                echo "},\n";
            } else {
                echo "Invalid FormatType $FormatType\n";
            }
            
            if ($this->CurrentRecord == $this->NumRecords - 1) { // This is the last one so finish everything up
                if ($FormatType == $XMLFormat) {
                    echo "\t</Records>\n";
                } elseif ($FormatType == $JSONFormat) {
                    echo "}\n";
                    echo "}\n";
                }
            }
        }
    }
